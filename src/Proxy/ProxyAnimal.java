package Proxy;

/**
 * A proxy animal object that creates the real animal only when description needs to be stored in the database.
 * @author Asma,Salman
 *
 */
public class ProxyAnimal implements Animal {
	
	private int id;
	private String name;
	private RealAnimal realAnimal;
	
	public ProxyAnimal(String name){
		setName(name);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;		
	}

	@Override
	public Description getDescription() {
		if(realAnimal!=null){
			System.out.println("Calling real animal's getDescription");
			return realAnimal.getDescription();
		}
		else
			return null;
	}

	@Override
	public void setDescription(Description description) {
		System.out.println("Creating the RealAnimal object from within the ProxyClass");
		RealAnimal realAnimal = new RealAnimal(name,description);
		realAnimal.setId(id);
		this.realAnimal = realAnimal;
		
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

}
