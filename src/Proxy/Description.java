package Proxy;

/**
 * Description class that describes the animal. Instance variables: int age, String habitat and String diet
 * @author Asma
 *
 */
public class Description {
	private int id;
	private int age;
	private String Habitat;
	private String Diet;
	
	public Description(){}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getHabitat() {
		return Habitat;
	}
	public void setHabitat(String habitat) {
		Habitat = habitat;
	}
	public String getDiet() {
		return Diet;
	}
	public void setDiet(String diet) {
		Diet = diet;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
