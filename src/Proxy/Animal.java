package Proxy;



/**
 * Proxy Interface for Animal
 * @author Asma,Salman
 *
 */
public interface Animal {
	public int getId();
	public void setId(int id);
	public String getName();
	public void setName(String name);
	public Description getDescription();
	public void setDescription(Description description);	
}
