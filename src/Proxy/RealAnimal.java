package Proxy;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Data.HibernateHelper;


/**
 * A real animal object that saves the description to the database on creation.
 * @author Asma,Salman
 *
 */

public class RealAnimal implements Animal {

	private int id;
	private String name;
	
	public RealAnimal(String name, Description description){
		System.out.println("RealAnimal constructor calling setDescription");
		setName(name);
		setDescription(description);
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public Description getDescription() {
		//retrieve description from database.
		System.out.println("Getting the description");
		try {
			Session session = HibernateHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			Query getFacilityQuery = session.createQuery("From Description where id=:id");		
			getFacilityQuery.setLong("id", id);
			List descList = getFacilityQuery.list();
			session.getTransaction().commit();
			return (Description)descList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public void setDescription(Description description) {
		//store description in database
		System.out.println("Setting the description");
		Session session = HibernateHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(description);
		session.getTransaction().commit();
	}
	
	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}
	
}
