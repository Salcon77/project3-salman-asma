package Observer;

/**
 * A subject interface
 * @author Asma,Salman
 *
 */
public interface Subject {
	public int register(Observer o);
	public void unregister(Observer o);
	public void notifyObserver();
}