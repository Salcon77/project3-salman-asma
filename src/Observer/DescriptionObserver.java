package Observer;

import java.util.ArrayList;

import Proxy.Animal;
import Proxy.Description;

/**
 * Observer that is responsible for printing the description of each animal.
 * @author Asma,Salman
 *
 */
public class DescriptionObserver implements Observer {
	
	private Subject descObserver;
	private int observerID;
	private ArrayList<Animal> animals;
	
	public DescriptionObserver(Subject descObserver){
		this.descObserver = descObserver;
		register();
	}

	@Override
	public void update(ArrayList<Animal> animals) {
		this.animals = animals;		
	}

	@Override
	public void register() {
		observerID = descObserver.register(this);
		System.out.println("new observer "+this.observerID);
	}
	
	public void PrintDescriptions(){
		System.out.println("Observer Id:: "+observerID +"\n");
		for(Animal a: animals){
			Description desc = a.getDescription();
			if(desc != null){
				System.out.println("Printing the "+a.getName()+"'s description. \n"
						+"\tAge: "+desc.getAge()
						+"\n\tDiet: "+desc.getDiet()
						+"\n\tHabitat: "+desc.getHabitat()+".\n");				
			}
			else{
				System.out.println("This "+a.getName()+" does not have a description associated with it (Proxy Animal).\n");
			}
		}
	}

}
