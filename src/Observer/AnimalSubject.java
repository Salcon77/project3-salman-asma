package Observer;
import java.util.ArrayList;

import Proxy.Animal;



public class AnimalSubject implements Subject {
	private ArrayList<Observer> observers;
	private ArrayList<Animal> animals;
	private static int observerIDTracker = 0;
	
	public AnimalSubject(){
		observers = new ArrayList<Observer>();
		animals = new ArrayList<Animal>();
	}

	@Override
	public int register(Observer NewO) {
		observers.add(NewO);
		++observerIDTracker;
		return observerIDTracker;
	}

	@Override
	public void unregister(Observer deleteO) {
		int observerIndex = observers.indexOf(deleteO);
		System.out.println("Observer"+(observerIndex+1)+" deleted");
		observers.remove(observerIndex);		
	}

	@Override
	public void notifyObserver() {
		for(Observer observer : observers){
			observer.update(animals);
		}
	}
	
	public void addAnimal(Animal animal){
		animals.add(animal);
		notifyObserver();
	}
}
