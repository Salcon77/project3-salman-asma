package Observer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import Proxy.Animal;


/**
 * Observer that keeps count of the animals
 * @author Asma,Salman
 *
 */
public class AnimalObserver implements Observer {
	private int observerID;
	private Subject animalObserver;
	private ArrayList<Animal> animals;
	private HashMap<String,Integer> AnimalCountMap;
	
	public AnimalObserver(Subject animalObserver){
		this.animalObserver = animalObserver;
		register();
		AnimalCountMap = new HashMap<String,Integer>();
	}
	
	@Override
	public void register(){
		observerID = animalObserver.register(this);
		System.out.println("new observer "+this.observerID);
	}

	@Override
	public void update(ArrayList<Animal> animals) {
		this.animals = animals;
		UpdateCount();
	}
	
	public void AnimalCount(){
		System.out.println("Observer Id:: "+observerID +"\n");
		if(!AnimalCountMap.isEmpty()){
			Iterator<Entry<String, Integer>> it = AnimalCountMap.entrySet().iterator();
		    while (it.hasNext()) {
		    	Map.Entry<String,Integer> pairs = (Map.Entry<String,Integer>)it.next();
		        System.out.println("The number of "+ pairs.getKey() +"'s are "+ pairs.getValue() +".\n");
		    }
		}
		
	}
	
	public void UpdateCount(){
		AnimalCountMap.clear();
		for(Animal a: animals){
			if(AnimalCountMap.containsKey(a.getName())){
				AnimalCountMap.put(a.getName(),AnimalCountMap.get(a.getName())+1);
			}
			else{
				AnimalCountMap.put(a.getName(),1);
			}
		}		
	}

}
