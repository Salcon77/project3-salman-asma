package Observer;
import java.util.ArrayList;

import Proxy.Animal;


/**
 * The observer interface
 * @author Asma,Salman
 *
 */
public interface Observer{
	public void update(ArrayList<Animal> animals);
	public void register(); 
}