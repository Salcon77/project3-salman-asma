package Client;
import Observer.AnimalObserver;
import Observer.AnimalSubject;
import Observer.DescriptionObserver;
import Proxy.Animal;
import Proxy.Description;
import Proxy.ProxyAnimal;

/**
 * A client for testing the proxy and the observer patterns.
 * @author Asma,Salman
 *
 */
public class AnimalClient {
	public static void main (String[] args){
		AnimalSubject x = new AnimalSubject();
		AnimalObserver observer = new AnimalObserver(x);
		
		
		System.out.println("\n\nTESTING THE OBSERVER PATTERN BY USING PROXY ANIMALS: ");
		System.out.println("Adding two penguins and one zebra to the subject and calling the count in the observer\n");
		Animal penguin = new ProxyAnimal("Penguin");
		Animal zebra = new ProxyAnimal("Zebra");
		Animal penguin2 = new ProxyAnimal("Penguin");
		
		x.addAnimal(penguin);
		x.addAnimal(zebra);
		x.addAnimal(penguin2);
		observer.AnimalCount();
		
		System.out.println("\n\nTESTING THE PROXY PATTERN\n");
		DescriptionObserver descObserver = new DescriptionObserver(x);
		Animal penguin3 = new ProxyAnimal("Penguin");
		penguin3.setId(1);
		
		//Saving the description which would create the real object.
		Description penguinDesc = new Description();
		penguinDesc.setId(1);
		penguinDesc.setAge(2);
		penguinDesc.setDiet("Fish");
		penguinDesc.setHabitat("Extremely cold regions");
		penguin3.setDescription(penguinDesc);
		
		//Another zebra created without a description. Saves time as description is not saved in the database.
		Animal zebra2 = new ProxyAnimal("Zebra");
		zebra2.setId(2);
		
		System.out.println("Adding 1 penguin with a description and a second zebra without a description to the subject");
		x.addAnimal(penguin3);
		x.addAnimal(zebra2);
		
		System.out.println("New Count of Animals: \n");
		observer.AnimalCount();
		System.out.println("Calling the print description in the description observer");
		descObserver.PrintDescriptions();	
	}

}
